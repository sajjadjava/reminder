package work_with_files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.BufferedReader;
import java.nio.file.Files;
import java.io.IOException;
import javax.xml.bind.JAXB;
import java.lang.Class;
import identity.*;

public class load_list_of_identity implements load_list_of_identity_interface{
    public list_of_identity load_list_of_identity(Path file_path) {
        try (BufferedReader input = Files.newBufferedReader(file_path)) {
            list_of_identity entering = JAXB.unmarshal(input,list_of_identity.class);
            return entering;
        } catch (IOException e) {
            e.printStackTrace();
            list_of_identity urgency = null;
            return urgency;
        }
    }
}
