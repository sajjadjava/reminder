import Graphical_User_Interfaces.*;
import identity.*;
import modules.*;
import Tasks.*;
import work_with_files.*;
import java.util.Scanner;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.io.PrintWriter;
import java.io.IOException;
public class Main {
    public static void main(String[] args) throws IOException
    {
        Scanner in = new Scanner(System.in);
        while(2 == 2) {
            begin_class begin = new begin_class();
            begin.begin();
            load_list_of_identity getter = new load_list_of_identity();
            int entering = in.nextInt();
            if (entering == 1) {
                signup_and_signin_class first = new signup_and_signin_class();
                identifier person = first.signup_design_and_setting();
                list_of_identity getting = getter.load_list_of_identity(Paths.get("src/com/ap/reminder/list_of_identity.fxml"));
                check_in check = new check_in();
                if(check.check_in(getting,person)) {
                    after_signin after = new after_signin();
                    load_list_of_fulltask load =new load_list_of_fulltask();
                    list_of_fulltask owner = load.load_list_of_full_task(Paths.get(String.format("src/com/ap/reminder/%s%s",person.getusername(),".fxml")));
                    after.task_list(owner);
                    int status = in.nextInt();
                    if(status == 0) {
                        save storage = new save();
                        storage.save(Paths.get(String.format("src/com/ap/reminder/%s%s", person.getusername(), ".fxml")), owner);
                        continue;
                    }
                    if(status == 1)
                    {
                        add_task adding = new add_task();
                        adding.add_task(owner);
                        save storage = new save();
                        storage.save(Paths.get(String.format("src/com/ap/reminder/%s%s", person.getusername(), ".fxml")), owner);
                        continue;
                    }
                    if(status == 2)
                    {
                        edit_or_remove edit = new edit_or_remove();
                        edit.edit_or_remove(owner);
                        save storage = new save();
                        storage.save(Paths.get(String.format("src/com/ap/reminder/%s%s", person.getusername(), ".fxml")), owner);
                        continue;
                    }
                }
                else {
                    System.err.println("you username or password is incorrect");
                    continue;
                }
            }
            if (entering == 2) {
                signup_and_signin_class first = new signup_and_signin_class();
                identifier person = first.signup_design_and_setting();
                list_of_identity getting = getter.load_list_of_identity(Paths.get("src/com/ap/reminder/list_of_identity.fxml"));
                check_save_identity check = new check_save_identity();
                if (!(check.existing_identifier(getting,person)))
                {
                    PrintWriter create_file = new PrintWriter(String.format("%s%s%s","src/com/ap/reminder/",person.getusername(),".fxml"));
                    create_file.printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                            "\n" +
                            "<?import java.lang.*?>\n" +
                            "<?import java.util.*?>\n" +
                            "<?import javafx.scene.*?>\n" +
                            "<?import javafx.scene.control.*?>\n" +
                            "<?import javafx.scene.layout.*?>\n" +
                            "\n" +
                            "<AnchorPane xmlns=\"http://javafx.com/javafx\"\n" +
                            "            xmlns:fx=\"http://javafx.com/fxml\"\n" +
                            "            fx:controller=\"%s\"\n" +
                            "            prefHeight=\"400.0\" prefWidth=\"600.0\">\n" +
                            "\n" +
                            "</AnchorPane>",person.getusername());
                    create_file.flush();
                }
                check.save_identity(getting, person);
                save storage = new save();
                storage.save(Paths.get("src/com/ap/reminder/list_of_identity.fxml"), getting);
                continue;
            }
        }
    }
}
