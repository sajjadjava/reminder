package work_with_files;
import Tasks.*;
import java.nio.file.Path;
import java.nio.file.Files;
import java.io.BufferedReader;
import java.io.IOException;
import javax.xml.bind.JAXB;

public class load_list_of_fulltask implements load_list_of_fulltask_interface{
    public list_of_fulltask load_list_of_full_task(Path file_path)
    {
        try(BufferedReader input = Files.newBufferedReader(file_path))
        {
            list_of_fulltask tasks = JAXB.unmarshal(input,list_of_fulltask.class);
            return tasks;
        }
        catch(IOException e)
        {
            e.printStackTrace();
            list_of_fulltask urgency = null;
            return urgency;
        }
    }
}
