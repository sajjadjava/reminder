package work_with_files;
import java.nio.file.Path;
import identity.*;

public interface load_list_of_identity_interface {
    public abstract list_of_identity load_list_of_identity(Path file_path);
}
