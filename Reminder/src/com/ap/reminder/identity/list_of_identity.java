package identity;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;
import java.util.ArrayList;

public class list_of_identity {
    @XmlElement(name="ID")
    private List<identifier> list_of_identity = new ArrayList<>();
    public List<identifier> getlist_of_identity()
    {
        return list_of_identity;
    }
}
