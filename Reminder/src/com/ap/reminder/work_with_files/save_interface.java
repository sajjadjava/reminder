package work_with_files;
import java.nio.file.Path;

public interface save_interface {
    public abstract <E> void save(Path file_path,E data);
}
