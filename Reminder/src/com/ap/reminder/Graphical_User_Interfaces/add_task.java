package Graphical_User_Interfaces;
import Tasks.*;
import java.util.Scanner;

public class add_task implements add_task_interface {
    public void add_task(list_of_fulltask user_tasks)
    {
        full_task new_task = new full_task();
        Scanner in = new Scanner(System.in);
        System.out.print("\033[H\033[2J");
        System.out.flush();
        System.out.print("enter the title of task: ");
        String title = in.next();
        new_task.settitle(title);
        System.out.println();
        System.out.print("enter the description of task: ");
        String description = in.next();
        new_task.setdescription(description);
        System.out.println();
        System.out.print("enter the task_status 0f task: ");
        String task_status = in.next();
        new_task.settask_status(task_status);
        System.out.println();
        System.out.print("enter the Date of task (for example: 2021/3/3 ): ");
        String Date = in.next();
        new_task.setDate(Date);
        System.out.println();
        System.out.println("if you want to add checklist press 0 and if your work is done press 1");
        while (in.nextInt() == 0)
        {
            checklist entering = new checklist();
            System.out.println();
            System.out.print("enter the title of checklist: ");
            String subject = in.next();
            entering.settitle(subject);
            System.out.println();
            System.out.print("enter the status of checklist (for example : true): ");
            boolean status = in.nextBoolean();
            entering.setcheck(status);
            System.out.println();
            new_task.getchecks().add(entering);
            System.out.println("if you want to add checklist press 0 and if your work is done press 1");
        }
        user_tasks.getlist().add(new_task);
    }
}
