package Graphical_User_Interfaces;
import Tasks.*;
import java.util.List;
import java.util.ArrayList;

public class showing_checklist implements showing_checklist_interface {
    public void showing_checklist(full_task user_task)
    {
        List<checklist> list = new ArrayList<>(user_task.getchecks());
        System.out.print("\033[H\033[2J");
        System.out.flush();
        for (int i=1;i<=list.size();i++)
        {
            System.out.printf("%d.title: %s%n",i,list.get(i-1).gettitle());
            System.out.printf("%d.check: %s%n",i,list.get(i-1).getcheck());
        }
    }
}
