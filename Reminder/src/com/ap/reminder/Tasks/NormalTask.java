package Tasks;

public class NormalTask {
    private String title;
    private String description;
    private String task_status;

    public String gettitle() {
        return title;
    }

    public void settitle(String title) {
        this.title = title;
    }

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    public String gettask_status() {
        return task_status;
    }

    public void settask_status(String task_status) {
        this.task_status = task_status;
    }
}
