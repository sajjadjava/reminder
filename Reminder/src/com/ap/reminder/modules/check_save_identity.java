package modules;
import identity.*;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;

public class check_save_identity implements check_save_identity_interface {
    public void save_identity(list_of_identity List,identifier ID)
    {
        if(!(existing_identifier(List,ID)))
        {
            List.getlist_of_identity().add(ID);
        }
    }
    public static boolean existing_identifier(list_of_identity List,identifier ID)
    {
        List<identifier> persons = new ArrayList<>(List.getlist_of_identity());
        for (int i=0;i<persons.size();i++)
        {
            identifier id = persons.get(i);
            if (id.getusername().equals(ID.getusername()))
                return true;
            if (id.getpassword() == ID.getpassword())
                return true;
        }
        return false;
    }
    public static boolean existing_identifiers(list_of_identity List,identifier ID)
    {
        List<identifier> persons = new ArrayList<>(List.getlist_of_identity());
        for (int i=0;i<persons.size();i++)
        {
            identifier id = persons.get(i);
            if (id.getusername().equals(ID.getusername()) && id.getpassword() == ID.getpassword())
                return true;
        }
        return false;
    }
}
