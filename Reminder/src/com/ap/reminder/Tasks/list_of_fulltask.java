package Tasks;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;
import java.util.ArrayList;

public class list_of_fulltask {
    @XmlElement(name="task")
    private List<full_task> list = new ArrayList<>();
    public List<full_task> getlist()
    {
        return list;
    }
}
