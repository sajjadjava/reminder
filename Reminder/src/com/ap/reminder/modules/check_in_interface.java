package modules;

import identity.identifier;
import identity.list_of_identity;

public interface check_in_interface {
    public abstract boolean check_in(list_of_identity List, identifier ID);
}
