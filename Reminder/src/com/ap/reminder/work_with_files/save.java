package work_with_files;
import java.nio.file.Path;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.io.IOException;
import javax.xml.bind.JAXB;

public class save implements save_interface {
    public <E> void save(Path file_path,E data)
    {
        try(BufferedWriter out = Files.newBufferedWriter(file_path))
        {
            JAXB.marshal(data,out);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
