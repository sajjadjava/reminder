package work_with_files;

import Tasks.list_of_fulltask;

import java.nio.file.Path;

public interface load_list_of_fulltask_interface {
    public abstract list_of_fulltask load_list_of_full_task(Path file_path);
}
