package Graphical_User_Interfaces;
import Tasks.*;
import java.util.Scanner;

public class edit_or_remove implements edit_or_remove_interface{
    public void edit_or_remove(list_of_fulltask user_tasks)
    {
        Scanner in = new Scanner(System.in);
        System.out.println();
        System.out.println("if you want to remove task press 0 and if you want to edit task press 1");
        System.out.print("press your number: ");
        int entering = in.nextInt();
        if (entering == 0)
        {
            System.out.println();
            System.out.print("press number of the removeable task: ");
            int number = in.nextInt();
            remove(user_tasks,number);
        }
        if (entering == 1)
        {
            System.out.println();
            System.out.print("press number of the editable task: ");
            int number = in.nextInt();
            edit(user_tasks,number);
        }
    }
    public void remove(list_of_fulltask user_tasks,int number)
    {
        user_tasks.getlist().remove(number-1);
    }
    public void edit(list_of_fulltask user_tasks,int number)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("\033[H\033[2J");
        System.out.flush();
        showing_fulltask play = new showing_fulltask();
        play.showing_fulltask(user_tasks.getlist().get(number-1));
        System.out.println();
        System.out.print("enter number of the editable element (if you want to start from first press 0 ): ");
        int entering = in.nextInt();
        while (entering !=0 && entering != 5)
        {
            System.out.println();
            if (entering == 1)
            {
                System.out.print("enter your title: ");
                String entrance = in.next();
                user_tasks.getlist().get(number-1).settitle(entrance);
                System.out.println();
                System.out.print("enter number of the editable element (if you want to start from first press 0 ): ");
                entering = in.nextInt();
            }
            if (entering == 2)
            {
                System.out.print("enter your description: ");
                String entrance = in.next();
                user_tasks.getlist().get(number-1).setdescription(entrance);
                System.out.println();
                System.out.print("enter number of the editable element (if you want to start from first press 0 ): ");
                entering = in.nextInt();
            }
            if (entering == 3)
            {
                System.out.print("enter your task_status: ");
                String entrance = in.next();
                user_tasks.getlist().get(number-1).settask_status(entrance);
                System.out.println();
                System.out.print("enter number of the editable element (if you want to start from first press 0 ): ");
                entering = in.nextInt();
            }
            if (entering == 4)
            {
                System.out.print("enter your Date: ");
                String entrance = in.next();
                user_tasks.getlist().get(number-1).setDate(entrance);
                System.out.println();
                System.out.print("enter number of the editable element (if you want to start from first press 0 ): ");
                entering = in.nextInt();
            }
        }
        if (entering == 5)
        {
            showing_checklist checking =new showing_checklist();
            checking.showing_checklist(user_tasks.getlist().get(number-1));
            System.out.print("enter number of the editable check (if you want to start from first press 0 ): ");
            int Number = in.nextInt();
            while (Number != 0)
            {
                System.out.println();
                System.out.print("enter your ideal title: ");
                String title = in.next();
                user_tasks.getlist().get(number-1).getchecks().get(Number-1).settitle(title);
                System.out.println();
                System.out.print("enter your ideal status: ");
                boolean check = in.nextBoolean();
                user_tasks.getlist().get(number-1).getchecks().get(Number-1).setcheck(check);
                System.out.println();
                System.out.print("enter number of the editable element (if you want to start from first press 0 ): ");
                Number = in.nextInt();
            }
        }
    }
}
